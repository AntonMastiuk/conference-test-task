# Conferences Viewer

## Build and deploy Conference Viewer Portlet
Having downloaded this repo, execute the following command to build the portlet and to move the output WAR file to
Liferay deploy dir  
`mvn clean package -pl wars/conferences-viewer-portlet -DbuildDirectory=/path/to/liferay/deploy`  
Start the Liferay server, navigate to localhost:8080 (or whatever port you are running Liferay on) click the edit button
in the header (pencil icon)  
![edit button](screenshots/edit.png)  
In the right sidebar, start typing Conference, and you'll be able to find a corresponding portlet. Drag it somewhere 
on a page.  
![find portlet](screenshots/find.png)  
On the top right corner of the portlet press the "three dots" icon and the press Configuration button  
![configuration button](screenshots/configuration.png)  
In a popup window enter the url of the Conference API and press sumbit.  
![configuration popup](screenshots/configuration-popup.png)  
Press publish, and you'll see that the data is fetched. Now you can use the portlet  
![data fetched](screenshots/data_fetched.png)
