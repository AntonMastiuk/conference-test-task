package conferences.viewer.portlet.dto;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;

public class ConferenceSearchForm {
    @NotBlank
    @Min(1)
    private String conferenceId;

    public String getConferenceId() {
        return conferenceId;
    }

    public void setConferenceId(String conferenceId) {
        this.conferenceId = conferenceId;
    }

    public Long conferenceAsLong() {
        return Long.valueOf(conferenceId);
    }
}
