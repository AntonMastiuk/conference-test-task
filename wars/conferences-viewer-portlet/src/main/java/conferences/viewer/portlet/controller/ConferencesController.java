package conferences.viewer.portlet.controller;

import com.liferay.portletmvc4spring.bind.annotation.ActionMapping;
import com.liferay.portletmvc4spring.bind.annotation.RenderMapping;
import conferences.viewer.portlet.dto.ConferenceDto;
import conferences.viewer.portlet.dto.ConferenceSearchForm;
import conferences.viewer.portlet.dto.ErrorAwareDto;
import conferences.viewer.portlet.service.ConferenceService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.portlet.ActionResponse;
import javax.portlet.MutableRenderParameters;
import javax.portlet.RenderRequest;

/**
 * @author amastiuk
 */
@Controller
@RequestMapping("VIEW")
public class ConferencesController {

    private final ConferenceService conferenceService;
    private final LocalValidatorFactoryBean localValidatorFactoryBean;

    private static final String CONFERENCE_SERVICE_URL_KEY = "conferenceServiceUrl";
    private static final String CONFERENCE_SERVICE_URL_DEFAULT_VALUE = "";
    private static final String ERROR_AWARE_DTO_KEY = "errorAwareDto";
    private static final String RENDER_PARAM_KEY = "javax.portlet.action";
    private static final String RENDER_PARAM_VALUE = "participants";
    private static final String RENDER_PARAM = RENDER_PARAM_KEY + "=" + RENDER_PARAM_VALUE;

    private String conferenceServiceUrl = CONFERENCE_SERVICE_URL_DEFAULT_VALUE;

    @ModelAttribute("searchForm")
    public ConferenceSearchForm getSearchForm() {
        return new ConferenceSearchForm();
    }

    public ConferencesController(ConferenceService conferenceService, LocalValidatorFactoryBean localValidatorFactoryBean) {
        this.conferenceService = conferenceService;
        this.localValidatorFactoryBean = localValidatorFactoryBean;
    }

    @RenderMapping
    public String prepareView(ModelMap modelMap, RenderRequest renderRequest) {
        conferenceServiceUrl = renderRequest.getPreferences().getValue(CONFERENCE_SERVICE_URL_KEY, CONFERENCE_SERVICE_URL_DEFAULT_VALUE);
        ErrorAwareDto<ConferenceDto[]> errorAwareConferences = conferenceService.getConferences(conferenceServiceUrl);
        modelMap.put(ERROR_AWARE_DTO_KEY, errorAwareConferences);
        return "conferences";
    }

    @RenderMapping(params = RENDER_PARAM)
    public String showParticipants() {
        return "users";
    }

    @ActionMapping
    public void searchForConference(@ModelAttribute("searchForm") ConferenceSearchForm searchForm, BindingResult bindingResult, ModelMap modelMap, ActionResponse actionResponse) {
        localValidatorFactoryBean.validate(searchForm, bindingResult);
        if (!bindingResult.hasErrors()) {
            ErrorAwareDto<ConferenceDto> errorAwareConference = conferenceService.getConferenceById(conferenceServiceUrl, searchForm.conferenceAsLong());
            modelMap.put(ERROR_AWARE_DTO_KEY, errorAwareConference);
            MutableRenderParameters mutableRenderParameters = actionResponse.getRenderParameters();
            mutableRenderParameters.setValue(RENDER_PARAM_KEY, RENDER_PARAM_VALUE);
        }
    }
}
