package conferences.viewer.portlet.service;

import conferences.viewer.portlet.dto.ConferenceDto;
import conferences.viewer.portlet.dto.ErrorAwareDto;

public interface ConferenceService {
    ErrorAwareDto<ConferenceDto[]> getConferences(String url);

    ErrorAwareDto<ConferenceDto> getConferenceById(String url, Long conferenceId);
}
