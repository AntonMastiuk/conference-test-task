package conferences.viewer.portlet.service.impl;

import com.liferay.portal.kernel.json.JSONException;
import com.liferay.portal.kernel.json.JSONFactoryUtil;
import com.liferay.portal.kernel.json.JSONObject;
import conferences.viewer.portlet.dto.ConferenceDto;
import conferences.viewer.portlet.dto.ErrorAwareDto;
import conferences.viewer.portlet.service.ConferenceService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.ResourceAccessException;
import org.springframework.web.client.RestTemplate;

import java.util.Locale;

@Service
public class ConferenceServiceImpl implements ConferenceService {

    private final RestTemplate restTemplate;
    private final MessageSource messageSource;

    private static final ConferenceDto[] DEFAULT_CONFERENCES_ARRAY = new ConferenceDto[]{};
    private static final ConferenceDto DEFAULT_CONFERENCE = new ConferenceDto();
    private static final String ERROR_MESSAGE = "";
    private static final String PARTICIPANTS_ENDPOINT = "/{conferenceId}/participants";
    private static final Logger logger = LoggerFactory.getLogger(ConferenceServiceImpl.class);

    public ConferenceServiceImpl(RestTemplate restTemplate, MessageSource messageSource) {
        this.restTemplate = restTemplate;
        this.messageSource = messageSource;
    }

    @Override
    public ErrorAwareDto<ConferenceDto[]> getConferences(String url) {
        return getForErrorAwareDto(url, ConferenceDto[].class, DEFAULT_CONFERENCES_ARRAY);
    }

    @Override
    public ErrorAwareDto<ConferenceDto> getConferenceById(String url, Long conferenceId) {
        return getForErrorAwareDto(url + PARTICIPANTS_ENDPOINT, ConferenceDto.class, DEFAULT_CONFERENCE, conferenceId);
    }

    private <T> ErrorAwareDto<T> getForErrorAwareDto(String url, Class<T> responseType, T defaultValue, Object... uriVariables) {
        String errorMessage = StringUtils.isEmpty(url) ? getMessage("errorMessage.emptyUrl") : ERROR_MESSAGE;
        T dto = defaultValue;
        if (!StringUtils.isEmpty(url)) {
            try {
                dto = restTemplate.getForObject(url, responseType, uriVariables);
            } catch (IllegalArgumentException | ResourceAccessException e) {
                errorMessage = getMessage("errorMessage.serviceUnreachable");
            } catch (HttpClientErrorException e) {
                if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                    errorMessage = extractErrorMessageFromJson(e.getResponseBodyAsString());
                } else {
                    errorMessage = getMessage("errorMessage.unexpectedError.withCode", e.getStatusCode().value());
                }
            }
        }
        if (!errorMessage.isEmpty()) {
            logger.error(errorMessage);
        }
        return new ErrorAwareDto<>(errorMessage, dto);
    }

    private String extractErrorMessageFromJson(String responseBodyAsString) {
        try {
            JSONObject jsonObject = JSONFactoryUtil.createJSONObject(responseBodyAsString);
            return jsonObject.has("errorMessage") ? jsonObject.getString("errorMessage") : getMessage("errorMessage.unexpectedError");
        } catch (JSONException ex) {
            return getMessage("errorMessage.invalidJsonResponse");
        }
    }

    private String getMessage(String key, Object... args) {
        return messageSource.getMessage(key, args, Locale.getDefault());
    }

}
