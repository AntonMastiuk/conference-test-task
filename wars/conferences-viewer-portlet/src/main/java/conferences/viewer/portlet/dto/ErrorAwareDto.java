package conferences.viewer.portlet.dto;

public class ErrorAwareDto<T> {
    private String errorMessage;
    private T dto;

    public ErrorAwareDto() {
    }

    public ErrorAwareDto(String errorMessage, T dto) {
        this.errorMessage = errorMessage;
        this.dto = dto;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public T getDto() {
        return dto;
    }

    public void setDto(T dto) {
        this.dto = dto;
    }
}
