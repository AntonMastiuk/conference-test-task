package conferences.viewer.portlet.dto;

import java.util.HashSet;
import java.util.Set;

public class ConferenceDto {
    private Long id;
    private String name;
    private int maxAvailableSeats;
    private boolean cancelled;
    private Set<UserDto> participants = new HashSet<>();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getMaxAvailableSeats() {
        return maxAvailableSeats;
    }

    public void setMaxAvailableSeats(int maxAvailableSeats) {
        this.maxAvailableSeats = maxAvailableSeats;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public void setCancelled(boolean cancelled) {
        this.cancelled = cancelled;
    }

    public Set<UserDto> getParticipants() {
        return participants;
    }

    public void setParticipants(Set<UserDto> participants) {
        this.participants = participants;
    }
}
