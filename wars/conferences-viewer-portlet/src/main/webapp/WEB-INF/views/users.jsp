<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>

<portlet:defineObjects/>
<portlet:renderURL var="home"/>
<div class="container">
    <p>
        <a href="${home}">
            <spring:message code="participants.backLink.title"/>
        </a>
        <span class="h1 ml-4 ${errorAwareDto.dto.cancelled ? 'text-muted' : ''}">
            <c:out value="${errorAwareDto.dto.name}"/>
            <c:if test="${empty errorAwareDto.errorMessage}">
                <spring:message code="conferences.booked-availableSeats.title" var="bookedAvailableSeatsTitle"/>
                <span class="badge badge-pill badge-secondary align-middle" title="${bookedAvailableSeatsTitle}">
                    <c:out value="${errorAwareDto.dto.participants.size()} / ${errorAwareDto.dto.maxAvailableSeats}"/>
                </span>
            </c:if>
            <c:if test="${errorAwareDto.dto.cancelled}">
                <span class="badge badge-pill badge-dark align-middle">
                    <spring:message code="conferences.status.cancelled"/>
                </span>
            </c:if>
        </span>
    </p>
    <c:if test="${empty errorAwareDto.dto.participants and empty errorAwareDto.errorMessage}">
        <div class="alert alert-primary" role="alert">
            <spring:message code="participants.emptyList"/>
        </div>
    </c:if>
    <c:if test="${not empty errorAwareDto.errorMessage}">
        <div class="alert alert-danger" role="alert">
            <c:out value="${errorAwareDto.errorMessage}"/>
        </div>
    </c:if>
    <ul class="list-group scrollable-list-group">
        <c:forEach var="user" items="${errorAwareDto.dto.participants}">
            <li class="list-group-item">
                <dl class="row">
                    <dt class="col-1">
                        <p class="h3">
                            <c:out value="${user.id}"/>
                        </p>
                    </dt>
                    <dd class="col-9">
                        <p class="h3">
                            <c:out value="${user.fullName}"/>
                        </p>
                    </dd>
                </dl>
            </li>
        </c:forEach>
    </ul>
</div>
