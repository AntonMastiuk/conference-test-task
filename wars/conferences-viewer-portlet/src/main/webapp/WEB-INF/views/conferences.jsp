<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<portlet:defineObjects/>
<portlet:actionURL var="mainFormActionURL"/>

<div class="container">
    <nav class="navbar navbar-expand-lg navbar-light">
        <h1 class="navbar-brand">
            <spring:message code="conferences.title"/>
        </h1>
        <div class="my-2">
            <div class="row">
                <form:form id="${namespace}mainForm" class="form-inline" method="post" modelAttribute="searchForm" action="${mainFormActionURL}">
                        <spring:message code="searchForm.placeholder" var="searchFormPlaceholder"/>
                        <form:input id="${namespace}conferenceId" path="conferenceId" cssClass="form-control" placeholder="${searchFormPlaceholder}"/>
                        <spring:message code="searchForm.button" var="searchFormButton"/>
                        <input class="btn btn-outline-info my-2 mx-3" value="${searchFormButton}" type="submit"/>
                </form:form>
            </div>
            <div class="row">
                <form:errors path="searchForm.conferenceId" cssClass="text-danger" />
            </div>
        </div>
    </nav>

    <div class="row">
        <div class="col-md">
            <c:if test="${empty errorAwareDto.dto and empty errorAwareDto.errorMessage}">
                <div class="alert alert-primary" role="alert">
                    <spring:message code="conferences.emptyList"/>
                </div>
            </c:if>
            <c:if test="${not empty errorAwareDto.errorMessage}">
                <div class="alert alert-danger" role="alert">
                    <c:out value="${errorAwareDto.errorMessage}"/>
                </div>
            </c:if>
            <ul class="list-group scrollable-list-group">
                <c:forEach var="conf" items="${errorAwareDto.dto}">
                    <li class="list-group-item">
                        <dl class="row">
                            <dt class="col-1">
                                <p class="h1">
                                    <c:out value="${conf.id}"/>
                                </p>
                            </dt>
                            <dd class="col-9">
                                <p class="h1 ${conf.cancelled ? 'text-muted' : ''}">
                                    <c:out value="${conf.name}"/>
                                    <c:if test="${conf.cancelled}">
                                        <span class="badge badge-pill badge-dark align-middle">
                                            <spring:message code="conferences.status.cancelled"/>
                                        </span>
                                    </c:if>
                                </p>
                                <p>
                                    <spring:message code="conferences.description.maxSeatsNumber" arguments="${conf.maxAvailableSeats}"/>
                                </p>
                            </dd>
                        </dl>
                    </li>
                </c:forEach>
            </ul>
        </div>
    </div>
</div>
