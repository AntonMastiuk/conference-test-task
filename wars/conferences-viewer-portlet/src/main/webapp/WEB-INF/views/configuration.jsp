<%@ page contentType="text/html;charset=UTF-8" %>

<%@ taglib prefix="liferay-portlet" uri="http://liferay.com/tld/portlet" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://xmlns.jcp.org/portlet_3_0" prefix="portlet" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<portlet:defineObjects/>
<liferay-portlet:actionURL portletConfiguration="true" var="configurationURL"/>
<div class="container">
    <div class="card mt-4">
        <div class="card-header">
            <h1 class="ml-2">
                <spring:message code="conferences.configuration.title"/>
            </h1>
        </div>
        <div class="card-body clearfix">
            <form:form id="${namespace}fm" class="m-2" method="post" action="${configurationURL}">
                <div>
                    <input type="hidden" name="${namespace}cmd" value="update">
                    <label for="conferenceServiceUrl">
                        <spring:message code="conferences.configuration.conferenceServiceUrl.lable"/>
                    </label>
                    <spring:message code="conferences.configuration.conferenceServiceUrl.input.placeholder" var="conferenceServiceUrlPlaceholder"/>
                    <input id="conferenceServiceUrl" type="text" name="${namespace}preferences--conferenceServiceUrl--" class="form-control"
                           placeholder="${conferenceServiceUrlPlaceholder}"
                           value="${portletPreferences.getValue("conferenceServiceUrl", "")}"/>
                    <input class="btn btn-outline-info my-2 float-right" type="submit"/>
                </div>
            </form:form>
        </div>
    </div>
</div>
